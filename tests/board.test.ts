import {Board} from '../src/board';
import { Square } from '../src/square';
import { Position } from '../src/position';
import { Player, Players } from '../src/player';



describe('board', () => {
    let size: number;
    let board: Board;
    const player: Player = new Player(Players.Terry);

    beforeAll(() => {
        size = 4;
        board = new Board(size, [player]);
    });

    it('constructor', () => {
        const b = board.getBoard();
        expect(b.length).toBe(size);
        for (let i = 0; i < size; i++) {
            expect(b[i].length).toBe(size);
        }
    });

    it('getSquare', () => {
        const s: Square = board.getSquare(new Position(0, 0));
        expect(s.getName()).toBe('0 0');
        expect(() => board.getSquare(new Position(size, 1)) ).toThrow();
        expect(() => board.getSquare(new Position(1, size)) ).toThrow();
        expect(() => board.getSquare(new Position(size, size)) ).toThrow();
    });

    it('getPlayerPosition', () => {
        expect(board.getPlayerPosition(player)).toEqual(new Position(0, 0));
        expect(() => { board.getPlayerPosition(new Player(Players.Terry)) }).toThrow();
    });
});
