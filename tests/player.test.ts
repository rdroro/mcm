import { Player, Players } from '../src/player';

describe('player', () => {
    it('constructor', () => {
        const player = new Player(Players.Terry);
        expect(player.name).toBe('Terry');
        expect(player.uuid).toBeDefined();
    });
});
