import { Square } from '../src/square';

describe('square', () => {



    const name = '0 1';
    let square: Square;

    beforeAll(() => {
        square = new Square(name);
    });

    it('constructor', () => {
        expect(square.name).toBe(name);
    });

    it('getName', () => {
        expect(square.getName()).toBe(name);
    });

    it('toString', () => {
        expect(square.toString()).toBe(name);
    });
});
