# mcm

Mage Contre mage - MCM - Est un jeu de plateau dans lequel des mages s'affrontent

# Get started

To dev environnement :

1. You need Docker installed
1. Pulled node image: `make update-docker-image`
1. To start env : `make start`

After than, you are in a standard node js env project so just:

1. `npm install`
1. To build project : `npn run tsc` (the tranpiled code is available in `build`
folder)

# Fonctionnement

Au monent de l'appel à game.start() on appelle game.startSinceEnd() qui délègue
à level.startTurn(). level.startTurn() est appelé autant de fois qu'il y a de
tour (valeur de game.nbTurn). Grâce au promise, on attend que chaque tour soit
terminé avant de lancer le suivant. C'est le même fonctionnement dans level.startTurn().
Le `level` est composé d'une pile de plugin à exécuter dans l'ordre. Chaque plugin
est exécuté et défini le moment où il a terminé son exécution. On détermine le
moment où le plugin a terminé son exécution en résolvant la promise du plugin
renvoyé par run.

Diagramme de séquence décrivant le texte ci-dessus
![sequence diagrame general execution](./doc/diagrams/export/general-execution.png)

# Draft

v0 :
* Jeu
    - Plateau
    - joueurs
    - Niveau
    + start
Niveau
    - plugins []
Plugin
    + init
    + destroy
    + run (1 méthode par phase)
    + next
    1. le plugin doit expliquer ce que l'utilisateur peut faire commme commande
* Case
    - position
    - objets []
* Plateau de jeu
    - taille
    - position des joueurs
    - quadrillage de case (tableau 2D de Case)
    + init avec la taille et la position des joueurs
    + déplacer un Joueur - abonnement action joueur déplacement
* Joueur
    - sac à dos
    + commande le déplacement - émet un evt de type action joueur
    + commande la récupération d'objets - émet un evt de type action joueur

Chaque action est enregistrée dans une pile d'exécution, pendant les phases du
jeu, les plugin vont consulter cette pile d'exécution.
Il peut y avoir des actions qui ont une durée de vie > au tour

Une action :
- type
- TTL : tour, nTour, partie

un niveau est définit par une succession de plugin

Les joueurs se déplacent
On verifie que c'est correct
on boucle
