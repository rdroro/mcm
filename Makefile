container_name = mcm
workdir = /opt/app

start:
	docker run -it --rm --name $(container_name)-node -v `pwd`:$(workdir) --workdir $(workdir) node:12-buster bash

update-docker-images:
	docker pull node:12-buster
