import { Game } from '../game';
import { Plugin } from '../plugin';

export class WaitPlayersAction extends Plugin {
    name = 'Wait Players Action';
    description = 'Wait Players action';

    run(): Promise<any> {
        console.log('WaitPlayersAction…');
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log('Player action OK');
                resolve('OK'); }, 1000);
        });
    }
}
