export abstract class Plugin {
    abstract name: string;
    abstract description: string;

    abstract run(): Promise<any>;

}
