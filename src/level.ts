import { Plugin } from './plugin';
import { WaitPlayersAction } from './plugins/wait-players-action';

export class Level {
    currentIndex = 0;
    plugins: Array<Plugin>;

    constructor() {
        this.plugins = [];
        this.addPlugin(new WaitPlayersAction());
        this.addPlugin(new WaitPlayersAction());
    }

    addPlugin(plugin: Plugin) {
        this.plugins.push(plugin);
    }

    startTurn(): Promise<any> {
            const promise = this.runPlugins();
            return promise.then(() => {
                console.log('End of tour');
                this.currentIndex = 0;
                return new Promise((resolve, reject) => {
                        resolve('ok');
                });
            });
    }

    runPlugins(): any {
        if (this.currentIndex < this.plugins.length) {
            const promise: Promise<any> = this.plugins[this.currentIndex].run();
            return promise.then(() => {
                this.currentIndex++;
                return this.runPlugins();
            });
        } else {
            return new Promise((resolve, reject) => {
                    resolve('ok');
            });
        }
    }
}
