import { Game } from './game';
import { Player, Players } from './player';

const player: Player = new Player(Players.Terry);
const game: Game = new Game([player], 4);

game.start();
