export class Player {
    name: string;
    uuid: number;
    constructor(name: string) {
        this.name = name;
        this.uuid = new Date().getTime();
    }
}

export enum Players {
    Terry = 'Terry',
    Mary = 'Mary',
    Robin = 'Robin',
    Elena = 'Elena',
}
