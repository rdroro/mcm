export class Square {
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    getName(): string {
        return this.name;
    }

    toString(): string {
        return this.name;
    }
}
