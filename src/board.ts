import { Square } from './square';
import { Player} from './player';
import { Position } from './position';
import { OutOfBoundPosition, UserNotFound } from './exceptions';

export class Board {
    playersPosition: any;
    size: number;
    board: Array<Array<Square>>;

    constructor(size: number, players: Array<Player>) {
        this.playersPosition = {};
        for (const player of players) {
            this.playersPosition[player.uuid] = new Position(0, 0);
        }
        this.size = size;
        this.board = new Array<Array<Square>>(this.size);
        for (let i = 0; i < this.size; i++) {
            this.board[i] = new Array<Square>(this.size);
            for (let j = 0; j < this.size; j++) {
                this.board[i][j] = new Square(i + ' ' + j);
            }
        }
    }

    getBoard(): Array<Array<Square>> {
        return this.board;
    }

    /**
     *  Return the square according to postion in parameter
     * @position the position
     * @return The square according to position in parameter
     * @throw OutOfBoundPosition
     */
    getSquare(position: Position): Square {
        const max = this.size - 1;
        if (position.x > max || position.y > max) {
            throw new OutOfBoundPosition();
        }
        return this.board[position.x][position.y];
    }

    /**
     * Return the position of the player in parameter
     * @param player
     * throw UserNotFound
     */
    getPlayerPosition(player: Player): Position {
        if (!this.playersPosition[player.uuid]) {
            throw new UserNotFound();
        }
        return this.playersPosition[player.uuid];
    }

}
