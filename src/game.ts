import {Board} from './board';
import {Level} from './level';
import {Player} from './player';
import { Position } from './position';

export class Game {
    static actions = [];

    board: Board;
    level: Level;
    private endGame = false;
    private nbTurn = 1;


    constructor(
        private players: Player[],
        private size: number
    ) {
        this.board = new Board(size, []);
        this.level = new Level();
    }

    start(): void {
        console.log('[Start Game, Yeah!]');

        const promise: Promise<any> = this.startSinceEnd();
        promise.then(() => {
            console.log('[End of Game]');
        });
    }

    startSinceEnd(): Promise<any> {
        if (this.nbTurn !== 0) {
            console.log('--- turn ' + this.nbTurn);
            this.nbTurn--;
            const promise = this.level.startTurn();
            return promise.then(() => {
                return this.startSinceEnd();
            });
        } else {
            return new Promise((resolve, reject) => {
                resolve('ok');
            });
        }
    }

    isOver(): boolean {
        return this.endGame;
    }
}
